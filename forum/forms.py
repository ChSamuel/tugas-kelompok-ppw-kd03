from django import forms
from .models import Commentsofwebsite

class Comment_Forum(forms.ModelForm):

    class Meta:
        model = Commentsofwebsite
        fields = ('name', 'message', )
        labels = {
            'name' : 'Username :',
            'message' : 'Your comment :',

        }