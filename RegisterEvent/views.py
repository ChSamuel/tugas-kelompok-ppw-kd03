from django.shortcuts import render, redirect
from .models import Jadwal
from .forms import JadwalForm

def register(request):
    if request.method == "POST":
        form = JadwalForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("success")
    else:
        form = JadwalForm()
    
    return render(request, "register.html", {'form' : form})

def success(request):
    return render(request, "success.html")