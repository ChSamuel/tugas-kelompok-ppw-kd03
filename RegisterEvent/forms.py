from django import forms
from .models import Jadwal

class DateInput(forms.DateInput):
    input_type="date"

class TimeInput(forms.TimeInput):
    input_type="time"
class JadwalForm(forms.ModelForm):
    class Meta:
        model = Jadwal
        fields = ('name', 'category', 'date', 'time', 'entranceFee')
        labels = {
                'name'          : "Event Name       :",
                'category'      : "Category         :",
                'date'          : "Date             :",
                'time'          : "Time             :",
                'entranceFee'   : "Entrance Fee     :",
            }
        widgets = {
            'date':DateInput,
            'time':TimeInput
        }