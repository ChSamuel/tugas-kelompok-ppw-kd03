from django.db import models
from django.utils import timezone
from datetime import datetime, date

CATEGORIES = (
    ('general','General'),
    ('bigdata', 'Big Data'),
    ('software','Software'),
    ('machinelearning','Machine Learning'),
    ('webdev','Web Development'),
)

# Create your models here.
class Jadwal(models.Model):
    name = models.CharField(max_length=50)
    category = models.CharField(max_length = 20, choices = CATEGORIES, default='general')
    date = models.DateField(default="2019-10-1")
    time = models.TimeField(default="12:00")
    entranceFee = models.IntegerField()