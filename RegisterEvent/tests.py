from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from datetime import date
from .models import Jadwal
from .forms import JadwalForm

class RegisterEventUnitTest(TestCase):
    def test_app_exists(self):
        response = Client().get('/RegisterEvent/')
        self.assertEqual(response.status_code, 200)

    def test_success_page_exists(self):
        response = Client().get('/RegisterEvent/success')
        self.assertEqual(response.status_code, 200)

    def test_create_jadwal(self):
        jadwal = Jadwal.objects.create(name = "Test", category = 'General', date='2000-1-1', time='21:00', entranceFee=10000)
        self.assertTrue(isinstance(jadwal, Jadwal))
        self.assertEqual(jadwal.category, "General")

    def test_form(self):
        form = JadwalForm()
        self.assertFalse(form.is_valid())

    def test_template(self):
        response = Client().get('/RegisterEvent/')
        self.assertTemplateUsed(response, 'register.html')
