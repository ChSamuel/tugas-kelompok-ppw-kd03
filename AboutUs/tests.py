from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .models import Feedback
from .forms import Input_Form

# Create your tests here.
class AboutUsUnitTest(TestCase):
    def test_app_exists(self):
        response = Client().get('/AboutUs/')
        self.assertEqual(response.status_code, 200)

    def test_send_working(self):
        response = Client().get('/AboutUs/send_feedback')
        self.assertEqual(response.status_code, 200)

    def test_submitted_working(self):
        response = Client().get('/AboutUs/submitted')
        self.assertEqual(response.status_code, 200)

    def test_send_feedback(self):
        feedback = Feedback.objects.create(name = "Anon", message = "...(some feedback)...")
        self.assertTrue(isinstance(feedback, Feedback))

    def test_validity(self):
        form = Input_Form()
        self.assertFalse(form.is_valid())

    def test_template(self):
        response = Client().get('/AboutUs/')
        self.assertTemplateUsed(response, 'about_us.html')
