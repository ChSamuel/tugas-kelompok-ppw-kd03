from django import forms
from .models import Feedback

class Input_Form(forms.ModelForm):

    class Meta:
        model = Feedback
        fields = '__all__'
        widgets = {
            'name' : forms.TextInput(
                attrs = {
                    'class' : 'form-control',
                    'size' : 20
                }
            ),

            'message' : forms.TextInput(
                attrs = {
                    'class' : 'form-control',
                    'size' : 80
                }
            ),
        }