from django.shortcuts import render
from RegisterEvent.models import Jadwal
from .models import Category
from .forms import CategoryForm


# Create your views here.
# def events(request):
#     return render(request,'events.html')

def searchevents(request):
    
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            form.save()
            events= []
            for event in Jadwal.objects.all():
                if event.category == form.cleaned_data['category']:
                    events.append(event)
            return render(request, "events.html", {'events':events})
    else:
        form = CategoryForm()
    
    
    return render(request, 'searchevents.html', {'form' : form})



def filter_events(request):
    events = Jadwal.objects.all()
    
    return render(request, 'events.html', {'events': events})

