from django.db import models
from datetime import datetime

# Create your models here.

CATEGORIES = (
    ('general','General'),
    ('bigdata', 'Big Data'),
    ('software','Software'),
    ('machinelearning','Machine Learning'),
    ('webdev','Web Development'),
)

class Category(models.Model):

    name = models.CharField(max_length=50)
    age = models.IntegerField()
    category = models.CharField(max_length = 20, choices = CATEGORIES, default='general')
    
    