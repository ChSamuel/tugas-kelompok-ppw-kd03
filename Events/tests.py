from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest

from .models import Category
from .forms import CategoryForm
from .views import searchevents, filter_events

class EventsUnitTest(TestCase):
    def test_check_app(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_check_object(self):
        self.assertFalse(Category.objects.all().exists())
        category1 = Category.objects.create(name="gafi", age="19", category = 'general')
        self.assertTrue(isinstance(category1, Category))
        self.assertEqual(category1.name, "gafi")

    def test_check_form(self):
        form = CategoryForm()
        self.assertFalse(form.is_valid())

    def test_views_methods(self):
        found = resolve('/events')
        self.assertEqual(found.func, filter_events)
        found = resolve('/')
        self.assertEqual(found.func, searchevents)


    def test_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'searchevents.html')
